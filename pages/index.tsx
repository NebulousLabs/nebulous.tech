import * as React from 'react'
import { Home } from '../src/components/Home'
import { Layout } from '../src/components/Layout'
import Head from 'next/head'
import { SITE_NAME } from '../src/constants/env'

export default props => (
  <div
    css={`
      background-color: rgba(52, 5, 107, 0.45);
      background: linear-gradient(315deg, rgba(7, 15, 41, 0.9) 0%, rgba(52, 5, 107, 0.7) 74%),
        url('/static/hero-neb.jpg');
      background-repeat: no-repeat;
      background-size: cover;
    `}
  >
    <Head>
      <title>{SITE_NAME}</title>
    </Head>
    <Layout>
      <Home />
    </Layout>
  </div>
)
