import 'isomorphic-fetch'
import * as React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { getStore } from '../src/store'
import reset from 'styled-reset'
import theme from '../theme'
import { indigo } from '../theme/colors'

const GlobalStyle = createGlobalStyle`
${reset}
html, body {
  height: 100%;
  font-family: Ubuntu, --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  background: rgba(52, 5, 107, 1);
  /* background-color: #5f72be;
  background-image: linear-gradient(90deg, #091744 0%, #5f72be 90%); */
  /* background-color: #29539b;
  background-image: linear-gradient(315deg, #311e8c 0%, #070f29 74%); */
}
`

export default class extends App {
  static async getInitialProps({ Component, router, ctx }) {
    const server = !!ctx.req
    const store = getStore(undefined, server)
    const state = store.getState()
    const out = { state, server } as any

    if (Component.getInitialProps) {
      return {
        ...out,
        pageProps: {
          ...(await Component.getInitialProps(ctx)),
        },
      }
    }

    return out
  }

  render() {
    const { props } = this as any
    const { Component, pageProps } = props

    return (
      <Container>
        <GlobalStyle />
        <ThemeProvider theme={theme}>
          <Provider store={getStore(undefined, props.server)}>
            <Component {...pageProps} />
          </Provider>
        </ThemeProvider>
      </Container>
    )
  }
}
