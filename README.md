# Nebulous.tech [![CircleCI](https://circleci.com/gh/deptno/next.js-typescript-starter-kit.svg?style=svg)](https://circleci.com/gh/deptno/next.js-typescript-starter-kit)

### Latest update

- typescript@3.3.3, next@8.0.0, react@16.8
