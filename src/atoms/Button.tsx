import { themeGet } from 'styled-system'
import { ReactChildren } from 'react'
import { Flex } from './Flex'
import { Box } from './Box'

export const Button = props => (
  <button
    css={`
      display: block;
      font-family: Ubuntu, --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu,
        Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      position: relative;
      cursor: pointer;
      white-space: nowrap;
      text-decoration: none;
      line-height: 1;
      padding: 18px 24px;
      border-radius: 0.25em;
      font-size: 16px;
      background: transparent;
      border: 1px solid white;
      color: white;
      svg {
        fill: white;
      }
      &:hover,
      &:active {
        border: 1px solid ${themeGet('colors.indigo.7')};
        color: ${themeGet('colors.indigo.7')};
        svg {
          fill: ${themeGet('colors.indigo.7')} !important;
        }
      }
    `}
    {...props}
  />
)

interface ButtonWithAdornmentProps {
  left?: any
  right?: any
  children: any
}

export const ButtonWithAdornment = ({
  left,
  right,
  children,
  ...props
}: ButtonWithAdornmentProps) => {
  return (
    <Button {...props}>
      <Flex justifyContent="center">
        {left && left}
        <Box mx="10px">{children}</Box>
        {right && right}
      </Flex>
    </Button>
  )
}
