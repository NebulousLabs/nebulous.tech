export const DEV = process.env.NODE_ENV !== 'production'

export const GA_TRACKING_ID = ''
export const FB_TRACKING_ID = ''
export const SENTRY_TRACKING_ID = ''

export const SITE_NAME = 'Nebulous'
export const SITE_TITLE = 'Nebulous'
export const SITE_DESCRIPTION =
  'Nebulous builds scalable, secure infrastructure for the decentralized internet. We believe that this new Internet will provide open access, enhanced privacy, and reduced costs.'
