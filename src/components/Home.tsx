import * as React from 'react'

import { Box } from '../atoms/Box'
import { Container } from '../atoms/Container'
import { H1, H2, Text, HeaderCaps } from '../atoms/Text'
import { Flex } from '../atoms/Flex'
import { themeGet } from 'styled-system'
import { HeaderHeight } from './Header'
import { FooterHeight } from './Footer'
import { Button, ButtonWithAdornment } from '../atoms/Button'
import { Grid } from '../atoms/Grid'
import { SkynetSVG, SiaSVG, TwitterSVG, DiscordSVG, GlobeSVG } from '../atoms/SVG'
import { HideWhenSmall, OnlyMobile } from '../atoms/ResponsiveTools'
import { devices } from '../../theme/breakpoints'

const combinedHeight = HeaderHeight + FooterHeight

const HomeHeight = `calc(100vh - ${combinedHeight}px)`

const Emphasized = (props) => (
  <span
    css={`
      color: ${themeGet('colors.indigo.7')};
    `}
    {...props}
  />
)

const ExternalLink = (props) => (
  <a
    css={`
      display: block;
      text-decoration: none;

      & > button {
        width: 100%;
      }
      @media ${devices.tablet} {
        & > button {
          width: auto;
        }
      }
      &:hover,
      &:active {
        text-decoration: none;
      }
    `}
    target="_blank"
    {...props}
  />
)

export const Home: React.FunctionComponent = (props) => (
  <div>
    <Container height="100%">
      <Box maxWidth={7} mt={[6, 8]}>
        <H1
          fontSize={['60px', '84px']}
          fontFamily="heading"
          color="indigo.9"
          lineHeight={1.1}
          fontWeight={5}
          mb={2}
        >
          The Future is <Emphasized>Nebulous</Emphasized>
        </H1>
        <Text fontSize={4} color="indigo.9">
          Nebulous builds <Emphasized>uncompromising</Emphasized> software infrastructure for the
          Decentralized Internet. We believe this new Internet will provide open access, enhanced
          privacy, and reduced costs.
        </Text>
        <Box py={6}>
          <ExternalLink href="https://jobs.lever.co/nebulous">
            <ButtonWithAdornment>Join Our Team</ButtonWithAdornment>
          </ExternalLink>
        </Box>
      </Box>
      <Box maxWidth={7} py={8}>
        <HeaderCaps color="indigo.9" pb={6}>
          Our Projects
        </HeaderCaps>
        <Box pb={8}>
          <Box>
            <Box>
              <Flex alignItems="center" justifyContent="space-between" pb={4}>
                <H1 fontSize={[7, 8]} color="indigo.9" pb={0}>
                  Sia
                </H1>
                <Box pr={4}>
                  <SiaSVG width="60px" height="60px" />
                </Box>
              </Flex>
              <Text fontSize={[3, 4]} color="indigo.8">
                Sia is the leading decentralized cloud storage platform. We leverage blockchain
                technology to create a data storage marketplace that is more robust and affordable
                than traditional cloud storage providers.
              </Text>
            </Box>
            <Box maxWidth={6} pt={6}>
              <Grid gridTemplateColumns={['1fr', 'repeat(3,1fr)']} gridGap={3}>
                <ExternalLink href="https://sia.tech/">
                  <ButtonWithAdornment left={<GlobeSVG />}>Website</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://twitter.com/siatechhq">
                  <ButtonWithAdornment left={<TwitterSVG />}>Twitter</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://discordapp.com/invite/sia">
                  <ButtonWithAdornment left={<DiscordSVG />}>Discord</ButtonWithAdornment>
                </ExternalLink>
              </Grid>
            </Box>
          </Box>
        </Box>
        <Box pb={8}>
          <Box>
            <Box>
              <Flex alignItems="center" justifyContent="space-between" pb={4}>
                <H1 fontSize={[7, 8]} color="indigo.9" pb={0}>
                  Skynet
                </H1>
                <Box pr={4}>
                  <SkynetSVG width="60px" height="60px" />
                </Box>
              </Flex>
              <Text fontSize={[3, 4]} color="indigo.8">
                The decentralized CDN and file sharing platform for devs. Skynet is the storage
                foundation for a Free Internet!
              </Text>
            </Box>
            <Box maxWidth={6} pt={6}>
              <Grid gridTemplateColumns={['1fr', 'repeat(3,1fr)']} gridGap={3}>
                <ExternalLink href="https://siasky.net">
                  <ButtonWithAdornment left={<GlobeSVG />}>Website</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://github.com/NebulousLabs/skynet-webportal">
                  <ButtonWithAdornment left={<TwitterSVG />}>GitHub</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://discordapp.com/invite/sia">
                  <ButtonWithAdornment left={<DiscordSVG />}>Sia Discord</ButtonWithAdornment>
                </ExternalLink>
              </Grid>
            </Box>
          </Box>
        </Box>
        <Box pb={8}>
          <Box>
            <Box>
              <Flex alignItems="center" justifyContent="space-between" pb={4}>
                <H1 fontSize={[7, 8]} color="indigo.9" pb={0}>
                  SiaStream
                </H1>
                <Box pr={4}>
                  <img src="/static/siastream.png" style={{ width: 60 }} />
                </Box>
              </Flex>
              <Text fontSize={[3, 4]} color="indigo.8">
                The perfect home for your media. Enjoy super-cheap storage for your media files,
                completely under your control. SiaStream uses next-gen cloud storage technology to
                provide fast streaming at the lowest costs. Optimized for Plex.
              </Text>
            </Box>
            <Box maxWidth={6} pt={6}>
              <Grid gridTemplateColumns={['1fr', 'repeat(3,1fr)']} gridGap={3}>
                <ExternalLink href="https://siastream.tech/">
                  <ButtonWithAdornment left={<GlobeSVG />}>Website</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://gitlab.com/NebulousLabs/siastream">
                  <ButtonWithAdornment left={<TwitterSVG />}>GitLab</ButtonWithAdornment>
                </ExternalLink>
                <ExternalLink href="https://discordapp.com/invite/sia">
                  <ButtonWithAdornment left={<DiscordSVG />}>Sia Discord</ButtonWithAdornment>
                </ExternalLink>
              </Grid>
            </Box>
          </Box>
        </Box>
      </Box>
    </Container>
  </div>
)
