import * as React from 'react'
import { Box } from '../atoms/Box'
import { Text } from '../atoms/Text'
import { Container } from '../atoms/Container'

export const FooterHeight = 180

export const Footer: React.FunctionComponent = props => (
  <Box as="footer" height={`${FooterHeight}px`}>
    <Container>
      <Box width={[1]}>
        <Text fontWeight={7} fontSize={4} color="indigo.9">
          Nebulous
        </Text>
        <Text color="indigo.9">
          67 Batterymarch St Floor 4<br /> Boston, MA 02110
        </Text>
        <Text color="indigo.7">hello@nebulous.tech</Text>
        <Box pt={4}>
          <Text color="indigo.9">© 2019 Nebulous, Inc.</Text>
        </Box>
      </Box>
    </Container>
  </Box>
)
